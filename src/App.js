import React, {useEffect, useReducer} from 'react';
import {Context} from './context';
import reducer from './reducer';
import AddTodo from './components/add-todo';
import TodoList from './components/todo-list';
import logo from './logo.svg';
import './App.css';

export default function App() {
  const [state, dispatch] = useReducer(reducer, JSON.parse(localStorage.getItem('todos-react')) || []);

  useEffect(() => {
    localStorage.setItem('todos-react', JSON.stringify(state));
  }, [state]);

  const addTodo = title => {
    dispatch({
      type: 'add',
      payload: title
    });
  };

  return (
    <Context.Provider value={{
      dispatch
    }}>
      <header className="App-header">
        <a href="https://ru.reactjs.org">
          <img src={logo} className="App-logo" alt="logo" />
        </a>
        <div>
          <h3>Todo list</h3>
        </div>
      </header>
      <div className="container">
        <AddTodo addTodo={addTodo}/>
        <TodoList todos={state} />
      </div>
    </Context.Provider>
  );
}
