import React, {useState} from 'react';

function AddTodo({addTodo}) {
  const [todoTitle, setTodoTitle] = useState('');

  const addTodoItem = e => {
    if (e.key === 'Enter') {
      addTodo(todoTitle);

      setTodoTitle('');
    }
  };

  return (
    <div className="add-todo">
      <input
        type="text"
        placeholder="Add todo"
        value={todoTitle}
        onChange={e => setTodoTitle(e.target.value)}
        onKeyPress={addTodoItem}
      />
    </div>
  );
}

export default AddTodo;
