import React, {useState, useContext} from 'react';
import {Context} from '../../context';

export default function TodoItem({title, id, completed}) {
  const {dispatch} = useContext(Context);
  const [edit, setEdit] = useState(false);
  const [newTodoTitle, setNewTodoTitle] = useState(title);
  const cls = ['todo'];

  if (completed) {
    cls.push('completed');
  }

  const toggle = () => dispatch({
    type: 'toggle',
    payload: id
  });

  const remove = () => dispatch({
    type: 'remove',
    payload: id
  });

  const update = () => setEdit(true);

  const addNewTodoItem = e => {
    if (e.key === 'Enter') {
      dispatch({
        type: 'edit',
        payload: {
          id,
          title: newTodoTitle
        }
      });

      setEdit(false);
    }
  };

  return (
    <li className={cls.join(' ')}>
      {
        !edit ?
          <label>
            <input
              type="checkbox"
              checked={completed}
              onChange={toggle}
            />
            <span onDoubleClick={update}>{title}</span>
            <div>
              <i
                className="material-icons red-text"
                onClick={remove}
              >
                delete
              </i>
              <i
                className="material-icons green-text edit"
                onClick={update}
              >
                edit
              </i>
            </div>
          </label> :
          <input
            type="text"
            placeholder="Edit"
            value={newTodoTitle}
            onChange={e => setNewTodoTitle(e.target.value)}
            onKeyPress={addNewTodoItem}
          />

      }
    </li>
  );
};
