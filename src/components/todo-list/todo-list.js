import React from 'react';
import Todo from '../todo';

export default function TodoList({todos}) {
  return (
    <ul>
      {todos.map(item => <Todo key={item.id} {...item} />)}
    </ul>
  )
};
